### Library

`build.gradle.kts:`

```
repositories {
    // com.jespage:aws-lambda-api
    maven("https://gitlab.com/api/v4/projects/45107195/packages/maven")
    // com.jespage:aws-lambda-logging
    maven("https://gitlab.com/api/v4/projects/56906418/packages/maven")
}

dependencies {
    implementation("com.jespage:aws-lambda-api:<version>")
}
```