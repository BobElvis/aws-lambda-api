plugins {
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.serialization") version "1.8.20"
    `java-library`
    `maven-publish`
}

group = "com.jespage"
val artifactName = "aws-lambda-api"
version = "0.5.0"

repositories {
    mavenCentral()
    // com.jespage:aws-lambda-logging
    maven("https://gitlab.com/api/v4/projects/56906418/packages/maven")
}

java {
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(11)
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
    api("com.amazonaws:aws-lambda-java-core:1.2.3")
    implementation("io.ktor:ktor-http:2.3.10")

    // Logging:
    implementation("io.github.oshai:kotlin-logging-jvm:6.0.9")
    api("com.jespage:aws-lambda-logging:2.0.3")
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to artifactName,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    repositories {
        maven {
            val projectId = System.getenv("CI_PROJECT_ID")
            url = uri("https://gitlab.com/api/v4/projects/${projectId}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        create<MavenPublication>("library") {
            artifactId = artifactName
            from(components["java"])
        }
    }
}
