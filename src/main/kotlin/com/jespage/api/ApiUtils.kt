@file:Suppress("unused")

package com.jespage.api

import io.ktor.http.*
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.serializer

fun <T> ApiCallAny.readBody(deserializer: DeserializationStrategy<T>): T = request.bodyDecoded()?.let { body ->
    try {
        json.decodeFromString(deserializer, body)
    } catch (e: Exception) {
        throw BadRequestException("Invalid body.", cause = e)
    }
} ?: throw BadRequestException("Missing body.")

inline fun <reified T> ApiCallAny.readBody(): T = readBody(json.serializersModule.serializer())

fun ApiCallAny.requireQuery(key: String) = request.queryStringParameters[key]
    ?: throw BadRequestException("Missing parameter '$key'.")

fun ApiCallAny.match(method: HttpMethod, pathRegex: String): Boolean =
    request.method == method && pathRegex.toRegex().matches(request.path)

fun ApiCallAny.matchHead(pathRegex: String) = match(HttpMethod.Head, pathRegex)
fun ApiCallAny.matchGet(pathRegex: String) = match(HttpMethod.Get, pathRegex)
fun ApiCallAny.matchPost(pathRegex: String) = match(HttpMethod.Post, pathRegex)
fun ApiCallAny.matchDelete(pathRegex: String) = match(HttpMethod.Delete, pathRegex)
fun ApiCallAny.matchPut(pathRegex: String) = match(HttpMethod.Put, pathRegex)
fun ApiCallAny.matchOptions(pathRegex: String) = match(HttpMethod.Options, pathRegex)
fun ApiCallAny.matchPatch(pathRegex: String) = match(HttpMethod.Patch, pathRegex)

