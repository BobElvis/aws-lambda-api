package com.jespage.api

import io.ktor.http.*
import java.util.Base64

data class Request(
    val path: String,
    val method: HttpMethod,
    val headers: Headers,
    val cookies: Map<String, String>,
    val queryStringParameters: Map<String, String>,
    val requestContext: RequestContext,
    val body: String?,
    val isBase64Encoded: Boolean
) {
    companion object {
        private val base64 = Base64.getDecoder()
    }

    fun bodyDecoded() = body?.let { if (isBase64Encoded) base64.decode(it).decodeToString() else it }
}

data class RequestContext(
    val protocol: String,
    val accountId: String?,
    val sourceIp: String?,
    val userAgent: String?
)
