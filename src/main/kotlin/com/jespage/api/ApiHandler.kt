package com.jespage.api

import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.serialization.json.Json
import kotlin.time.Duration

abstract class GenericApiHandler(val json: Json) {
    companion object {
        private val logger = KotlinLogging.logger { }
    }

    open val cors: CorsOptions? = null
    open val unknownExceptionInfoInResponse: Boolean = false

    suspend fun handleRequestRaw(call: ApiCallAny, timeout: Duration) {
        logger.info { "${call.request}" }
        try {
            coroutineScope {
                val timeoutTask = async {
                    delay(timeout)
                    error("Timed out ($timeout)")
                }

                /** CORS **/
                cors?.let {
                    call.response.setCorsHeaders(it)
                    if (call.request.method == HttpMethod.Options) call.respond(HttpStatusCode.NoContent)
                    else handleRequest(call)
                } ?: run {
                    handleRequest(call)
                }
                timeoutTask.cancel()
            }
        } catch (e: Throwable) {
            if (call.responded) {
                logger.error(e) { "Request handler exception after call executed (ignoring)." }
            } else if (e is ApiException) {
                logger.info { "Request handler ${e::class.simpleName}: ${e.internalReason}" }
                call.respond(e.responseMessage, e.statusCode)
            } else {
                logger.error(e) { "Request handler unknown exception." }
                val msg = if (unknownExceptionInfoInResponse) e.unknownExceptionMessage() else "Internal Server Error"
                call.respond(msg, HttpStatusCode.InternalServerError)
            }
        }
    }

    abstract suspend fun handleRequest(call: ApiCallAny)
}