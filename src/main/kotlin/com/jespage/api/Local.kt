package com.jespage.api

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.runBlocking
import java.net.InetSocketAddress
import java.util.concurrent.Executors
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

private val logger = KotlinLogging.logger { }

fun HttpExchange.toRequest() = Request(
    path = requestURI.path.trimEnd('/'),
    method = HttpMethod.parse(requestMethod),
    headers = HeadersImpl(requestHeaders.filterKeys { it.lowercase() != "cookie" }),
    cookies = requestHeaders
        .filterKeys { it.lowercase() == "cookie" }
        .flatMap { it.value }
        .flatMap { it.split("; ") }
        .map { it.split("=", limit = 2) }
        .filter { it.size == 2 }
        .associate { it[0] to it[1] },
    queryStringParameters = requestURI.query
        ?.split("&")
        ?.map { it.split("=") }
        ?.associate { it[0] to it.getOrElse(1) { "" } }
        ?: mapOf(),
    requestContext = RequestContext(
        protocol = protocol,
        accountId = null,
        sourceIp = null,
        userAgent = null
    ),
    body = requestBody.reader().readText(),
    isBase64Encoded = false
)

class LocalCallExecutor(private val httpExchange: HttpExchange) : CallExecutor {
    override fun execute(call: ApiCallAny, status: HttpStatusCode, body: String) {
        val content = body.encodeToByteArray()
        httpExchange.run {
            call.response.headers.forEach { responseHeaders[it.key] = it.value }
            call.response.cookies.values.forEach { responseHeaders.add(HttpHeaders.SetCookie, it) }
            if (status == HttpStatusCode.NoContent) {
                sendResponseHeaders(status.value, -1)
            } else {
                sendResponseHeaders(status.value, content.size.toLong())
                responseBody.write(content)
                responseBody.flush()
                responseBody.close()
            }
        }
    }
}

@Suppress("unused")
fun startServer(
    handler: GenericApiHandler,
    port: Int = 8080,
    backlog: Int = 20,
    timeout: Duration = 5.minutes
): Unit = runBlocking {
    HttpServer.create(InetSocketAddress(port), backlog).apply {
        createContext("/") {
            runBlocking {
                handler.handleRequestRaw(ApiCallAny(handler.json, it.toRequest(), LocalCallExecutor(it)), timeout)
            }
        }
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
    }.start()
    logger.info { "Server started on port $port" }
    awaitCancellation()
}