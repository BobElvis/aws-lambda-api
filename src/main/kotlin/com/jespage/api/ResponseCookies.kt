package com.jespage.api

import io.ktor.http.*
import io.ktor.util.date.*

class ResponseCookies {
    val values = mutableListOf<String>()

    fun append(item: Cookie) {
        val header = renderSetCookieHeader(
            name = item.name,
            value = item.value,
            encoding = item.encoding,
            maxAge = item.maxAge,
            expires = item.expires,
            domain = item.domain,
            path = item.path,
            secure = item.secure,
            httpOnly = item.httpOnly,
            extensions = item.extensions,
            includeEncoding = false
        )
        values.add(header)
    }

    fun appendExpired(name: String, domain: String? = null, path: String? = null) {
        append(Cookie(name, "", domain = domain, path = path, expires = GMTDate.START))
    }
}