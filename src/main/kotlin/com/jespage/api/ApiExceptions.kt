package com.jespage.api

import io.ktor.http.*

open class ApiException(
    val statusCode: HttpStatusCode,
    val responseMessage: String,
    val internalReason: String,
    cause: Throwable? = null
) : Exception(internalReason, cause)

open class BadRequestException(responseMessage: String, internalReason: String? = null, cause: Throwable? = null) :
    ApiException(HttpStatusCode.BadRequest, internalReason ?: responseMessage, responseMessage, cause)

@Suppress("unused")
class UnauthorizedException(internalReason: String, responseMessage: String = "") :
    ApiException(HttpStatusCode.Unauthorized, responseMessage, internalReason)

@Suppress("unused")
class ForbiddenException(internalReason: String, responseMessage: String = "") :
    ApiException(HttpStatusCode.Forbidden, responseMessage, internalReason)

fun Throwable.unknownExceptionMessage(): String {
    val s = toString().split(":", limit = 2)
    return "${s[0].substringAfterLast(".", s[0])}:${s.getOrNull(1) ?: " ???"}"
}