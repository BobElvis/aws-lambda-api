@file:OptIn(ExperimentalSerializationApi::class)

package com.jespage.api

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import io.ktor.http.*
import com.jespage.logging.AwsLambdaLogger
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import kotlinx.serialization.json.encodeToStream
import java.io.InputStream
import java.io.OutputStream
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

/**
 * Source: https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-develop-integrations-lambda.html
 * Some fields are not included.
 */
@Serializable
data class AwsApiRequest(
    val rawPath: String,
    val cookies: List<String> = emptyList(),
    val headers: Map<String, String> = emptyMap(),
    val queryStringParameters: Map<String, String> = emptyMap(),
    val requestContext: RequestContext,
    val body: String? = null,
    val isBase64Encoded: Boolean = false
) {
    @Serializable
    data class RequestContext(
        val accountId: String,
        val http: Http
    ) {
        @Serializable
        data class Http(
            val method: String,
            val protocol: String,
            val sourceIp: String,
            val userAgent: String
        )
    }
}

@Serializable
data class AwsApiResponse(
    val isBase64Encoded: Boolean,
    val statusCode: Int,
    val headers: Map<String, String>,
    val body: String,
    val cookies: List<String> = listOf()
)

private fun parseCookies(raw: List<String>) = buildMap {
    raw.forEach {
        val split = it.split("=", limit = 2)
        if (split.size == 2) this[split[0]] = split[1]
    }
}

private fun AwsApiRequest.toRequest() = Request(
    path = rawPath,
    method = HttpMethod.parse(requestContext.http.method),
    headers = HeadersImpl(headers.mapValues { it.value.split(",") }),
    cookies = parseCookies(cookies),
    queryStringParameters = queryStringParameters,
    requestContext = RequestContext(
        protocol = requestContext.http.protocol,
        accountId = requestContext.accountId,
        sourceIp = requestContext.http.sourceIp,
        userAgent = requestContext.http.userAgent
    ),
    body = body,
    isBase64Encoded = isBase64Encoded
)

class AwsCallExecutor(private val output: OutputStream) : CallExecutor {
    override fun execute(call: ApiCallAny, status: HttpStatusCode, body: String) {
        call.json.encodeToStream(
            AwsApiResponse(
                statusCode = status.value,
                headers = call.response.headers,
                isBase64Encoded = false,
                body = body,
                cookies = call.response.cookies.values
            ), output
        )
        output.close()
    }
}

@Suppress("unused")
abstract class AwsApiHandler(
    json: Json,
    private val timeoutMargin: Duration = 10.seconds
) : RequestStreamHandler, GenericApiHandler(json) {

    /** AWS Request **/
    override fun handleRequest(input: InputStream, output: OutputStream, context: Context) {
        try {
            AwsLambdaLogger.awsRequestId = context.awsRequestId
            val request = json.decodeFromStream<AwsApiRequest>(input)
            val call = ApiCallAny(json, request.toRequest(), AwsCallExecutor(output))
            runBlocking {
                handleRequestRaw(call, context.remainingTimeInMillis.milliseconds - timeoutMargin)
            }
        } finally {
            input.close()
            output.close()
        }
    }
}
