package com.jespage.api

import io.ktor.http.*
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.seconds

data class CorsOptions(
    val origin: String,
    val methods: String? = "*",
    val headers: String? = "*",
    val credentials: Boolean? = true,
    val maxAge: Duration? = 5.seconds
)

fun Response.setCorsHeaders(
    origin: String?,
    methods: String? = "*",
    headers: String? = "*",
    credentials: Boolean? = true,
    maxAge: Duration? = 0.hours
) {
    origin?.let { this.headers[HttpHeaders.AccessControlAllowOrigin] = it }
    methods?.let { this.headers[HttpHeaders.AccessControlAllowMethods] = it }
    headers?.let { this.headers[HttpHeaders.AccessControlAllowHeaders] = it }
    credentials?.let { this.headers[HttpHeaders.AccessControlAllowCredentials] = it.toString() }
    maxAge?.let { this.headers[HttpHeaders.AccessControlMaxAge] = it.inWholeSeconds.toString() }
}

fun Response.setCorsHeaders(options: CorsOptions) = setCorsHeaders(
    origin = options.origin,
    methods = options.methods,
    headers = options.headers,
    credentials = options.credentials,
    maxAge = options.maxAge
)