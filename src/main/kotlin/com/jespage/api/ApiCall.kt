package com.jespage.api

import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

private val logger = KotlinLogging.logger { }

class Response {
    val headers = mutableMapOf<String, String>()
    val cookies = ResponseCookies()
}

interface CallExecutor {
    fun execute(call: ApiCallAny, status: HttpStatusCode, body: String)
}

interface ApiCall {
    val request: Request
    val response: Response
    fun execute(statusCode: HttpStatusCode, body: String)
}

class ApiCallAny(
    val json: Json,
    override val request: Request,
    private val executor: CallExecutor
) : ApiCall {
    companion object {
        val contentTypeTextUtf8 = ContentType.Text.Plain.withParameter("charset", "UTF-8")
    }

    override val response = Response()
    var responded: Boolean = false

    fun respond(statusCode: HttpStatusCode) {
        if (statusCode == HttpStatusCode.NoContent) execute(statusCode, "")
        else respond(statusCode.description, statusCode)
    }

    inline fun <reified T : Any> respond(message: T, statusCode: HttpStatusCode = HttpStatusCode.OK) {
        val (msg, contentType) = when (message) {
            is String -> message to contentTypeTextUtf8
            else -> json.encodeToString(message) to ContentType.Application.Json
        }
        response.headers[HttpHeaders.ContentType] = contentType.toString()
        execute(statusCode, msg)
        responded = true
    }

    override fun execute(statusCode: HttpStatusCode, body: String) {
        executor.execute(this, statusCode, body)
        logger.info { "$statusCode '${request.path}' H=${response.headers}, C=${response.cookies.values}, B=${body}" }
    }
}
